package com.eventhub.eventhub.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MessageModel {
	private String content;
}
