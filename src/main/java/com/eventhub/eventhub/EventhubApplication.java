package com.eventhub.eventhub;
import com.eventhub.eventhub.model.MessageModel;
import org.springframework.boot.CommandLineRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class EventhubApplication implements CommandLineRunner {

//	private static final Logger LOGGER = LoggerFactory.getLogger(EventhubApplication.class);
//	private static final Sinks.Many<Message<String>> many = Sinks.many().unicast().onBackpressureBuffer();
	public static void main(String[] args) {
		SpringApplication.run(EventhubApplication.class, args);
	}
	private static final Logger LOGGER = LoggerFactory.getLogger(EventhubApplication.class);
	private static final Sinks.Many<Message<MessageModel>> many = Sinks.many().unicast().onBackpressureBuffer();


//	@Bean
//	public Supplier<Flux<Message<String>>> supply() {
//		return ()->many.asFlux()
//				.doOnNext(m->LOGGER.info("Manually sending message {}", m))
//				.doOnError(t->LOGGER.error("Error encountered", t));
//	}
//
//	@Bean
//	public Consumer<Message<String>> consume() {
//		return message->{
//			Checkpointer checkpointer = (Checkpointer) message.getHeaders().get(CHECKPOINTER);
//			LOGGER.info("New message received: '{}', partition key: {}, sequence number: {}, offset: {}, enqueued "
//							+"time: {}",
//					message.getPayload(),
//					message.getHeaders().get(EventHubsHeaders.PARTITION_KEY),
//					message.getHeaders().get(EventHubsHeaders.SEQUENCE_NUMBER),
//					message.getHeaders().get(EventHubsHeaders.OFFSET),
//					message.getHeaders().get(EventHubsHeaders.ENQUEUED_TIME)
//			);
//			checkpointer.success()
//					.doOnSuccess(success->LOGGER.info("Message '{}' successfully checkpointed",
//							message.getPayload()))
//					.doOnError(error->LOGGER.error("Exception found", error))
//					.block();
//		};
//	}

//	@Override
//	public void run(String... args) {
//		LOGGER.info("Going to add message {} to sendMessage.", "Hello Word");
//		many.emitNext(MessageBuilder.withPayload("Hello Word").build(), Sinks.EmitFailureHandler.FAIL_FAST);
//	}

	@Bean
	public Supplier<Flux<Message<MessageModel>>> supply() {
		return () -> many.asFlux()
				.doOnNext(m -> LOGGER.info("Manually sending message {}", m.getPayload().getContent()))
				.doOnError(t -> LOGGER.error("Error encountered", t));
	}

	@Bean
	public Consumer<Message<MessageModel>> consume() {
		return message -> LOGGER.info("New message received: '{}'", message.getPayload().getContent());
	}

	@Override
	public void run(String... args) {
		many.emitNext(MessageBuilder.withPayload(new MessageModel("Hello, world!")).build(), Sinks.EmitFailureHandler.FAIL_FAST);
	}
}
